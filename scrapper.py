import imp
from operator import le
from platform import release
from xml.dom.minidom import Element
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time 
import pdb
driver = webdriver.Chrome('./chromedriver')
driver.get("https://movies123.world/")
driver.set_window_size(1920, 1080)
print("Website open successfully")
try:
    driver.execute_script("window.scrollTo(50,document.body.scrollHeight)")
    search_btn = driver.find_element_by_tag_name("div.hm-button a.btn.btn-lg.btn-successful")
    search_btn.click()
    WebDriverWait(driver,120).until(EC.presence_of_element_located(
    (By.ID, "main")))
    counter = 0
    all_movies = driver.find_elements_by_tag_name("div.ml-item")
    try:
      for movie in all_movies:
        try:
            all_movies = driver.find_elements_by_tag_name("div.ml-item")
            movie = all_movies[counter]
            WebDriverWait(driver,80).until(EC.presence_of_element_located(
            (By.ID, "main")))
            time.sleep(3)
            soup = BeautifulSoup(movie.get_attribute('innerHTML'), 'html.parser')
            quality = soup.select("span.mli-quality")[0].text
            print(quality)
            image_url = soup.select("img.lazy.thumb.mli-thumb")
            print(image_url)
            title = image_url[0]['alt']
            print(title)
            movie_image = image_url[0]['data-original']
            print(movie_image)
            movie_href  = soup.select("a.ml-mask.jt")[0]['href']
            print(movie_href)
            try:
                singe_movie_a = movie.find_element_by_tag_name("a.ml-mask.jt")
                time.sleep(3)
                singe_movie_a.click()
            except:
                WebDriverWait(driver,120).until(EC.presence_of_element_located(
                (By.ID, "main")))
                counter = counter + 1
                search_btn = driver.find_element_by_tag_name("div.hm-button a.btn.btn-lg.btn-successful")
                search_btn.click() 
            # pdb.set_trace()
            element =  WebDriverWait(driver,120).until(EC.presence_of_element_located(
            (By.ID, "main")))  
            # pdb.set_trace()        
            soup = BeautifulSoup(element.get_attribute('innerHTML'), 'html.parser')
            time.sleep(3)
            director = soup.select("div.mvici-left p")[2].text
            print(director)
            country = soup.select("div.mvici-left p")[3].text
            print(country)
            actors =  soup.select("div.mvici-left p")[1].text
            print(actors)
            geners = soup.select("div.mvici-left p")[0].text
            print(geners)
            duration = soup.select("div.mvici-right p")[0].text
            print(duration)
            release_date = soup.select("div.mvici-right p")[2].text
            print(release_date)
            rating = soup.select("div.mvici-right p")[3].text
            print(rating)
            description = soup.select("div.desc")[0].text
            print(description)
            element.find_element_by_tag_name("ol.breadcrumb li a").click()
            counter= counter + 1
        except:
            WebDriverWait(driver,120).until(EC.presence_of_element_located(
            (By.ID, "main")))
            counter = counter + 1
            search_btn = driver.find_element_by_tag_name("div.hm-button a.btn.btn-lg.btn-successful")
            search_btn.click() 
    except:
        print("Something went wrong")
finally:
  driver.close()